# Estudiando-la-esp32

## Organización del Repo
- `libs`: acá las librerías utilizadas en el proyecto
- `src`: acá los códigos fuente de cada ejemplo o prueba
- `doc`: acá la documentación descargada útil: pdfs, esquemáticos, etc
- `ejemplos`: ejemplos útiles
	- `logger-base`: en `esp32-data1.ino` código que funciona con módulo SD, RTC y OLED

## Objetivos

- Familiarizarse con la ESP32
    - Conocer voltajes, recursos, memoria, microprocesador, etc
    - Ley de Ohm, unidades
    - Probar programas simples: hola mundo, poner pines en alto/bajo, prender leds, salida por puerto serie/usb
- Evaluar modos de bajo consumo: analizar consumos en cada modo, pros y contra de cada uno
- Incorporar módulos externos:
    - [Módulo microSD](https://articulo.mercadolibre.com.ar/MLA-905429694-modulo-lector-memorias-micro-sd-arduino-5v-hobbytronica-_JM#position=1&search_layout=grid&type=item&tracking_id=4cf3b9a5-a99f-49db-bfd5-cb37c33ad5c2)
    - [Módulo RTC DS3231](https://articulo.mercadolibre.com.ar/MLA-916734561-modulo-rtc-ds3231-alta-precision-eeprom-24c32-arduino-cpila-_JM#position=1&search_layout=stack&type=item&tracking_id=58c5016a-7649-4315-9b14-0e64c211faeb)
    - [Display OLED 1.3](https://articulo.mercadolibre.com.ar/MLA-901290093-display-oled-13-azul-128x64-i2c-arduino-ssh1106-hobbytronic-_JM#position=1&search_layout=grid&type=item&tracking_id=6a46e029-a5ee-4c2d-a04e-5af39323d4dc)
    - Sensor de presión
- Evaluar sistemas de comunicación 
    - WiFi
    - Bluetooth

## Instalacion y configuracion
- ¿Qué instalar para programarla? en windows y en linux?
- dónde van las libs, detalles a tener en cuenta

## Análisis de consumo
- En el [readme de consumo](README-consumos.md)

## Links de interés
- [Proyectito en micropython](https://gitlab.com/emilopez/pycamp21-micropython)

