#include <RTClib.h>
//#include "Sodaq_DS3231.h"     //no anda con esp32
#include <Adafruit_SSD1306.h>
//#include "SSD1306Ascii.h" //no anda con esp32
//#include "SSD1306AsciiAvrI2c.h" //no anda con esp32
#include <Adafruit_GFX.h>    
#include <Adafruit_SSD1306.h>
//#include <Fonts/Font4x5Fixed.h> // include custom font   //C:\Users\Ryzen\Documents\Arduino\libraries\Adafruit_GFX_Library\Fonts
#include <Fonts/fuente.h> // include custom font   //C:\Users\Ryzen\Documents\Arduino\libraries\Adafruit_GFX_Library\Fonts

#include "SD.h"
#include "SPI.h"

//#include <LowPower.h>
#include <DHT.h>
#include <SoftwareSPI2.h>
#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>



const int chipSelect = SS;//10;

#define OLED_RESET 4

RTC_DS3231 rtc;
Adafruit_SSD1306 oled(OLED_RESET);
File myFile;

unsigned long tiempom; 


void setup() {
  Serial.begin (9600);
  oled.begin(SSD1306_SWITCHCAPVCC, 0x3c);                     
  oled.clearDisplay();
  oled.setFont(&SansSerif_bold_8);
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  oled.setCursor(0,0);
  oled.println("Arranca");
  oled.display();
  rtc.begin();
      //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  String ts = get_timestamp();
  oled.println(ts); 
  oled.display();
      
     SD.begin(SS);
     oled.println("SD Correcta"); 
     oled.display();
     delay(3000);
  /*   myFile = SD.open("/esp32.txt", FILE_WRITE);
     myFile.println("prueba linea1: 1, 2, 3.");
     oled.println("esto es una prueba");
     myFile.close();
   */      
     //agrega datos y si no existe lo crea
     myFile = SD.open("/esp32.txt", FILE_APPEND);
     myFile.println("Arranca ESP32");
     myFile.close();
     
      
    oled.display();
    delay(2000);
    /*
    myFile = SD.open("/esp32.txt");
    while(myFile.available()){
      Serial.write(myFile.read());
    }
    myFile.close();
   */     
   
  delay(2000);
  tiempom = millis();
  oled.clearDisplay();
}

void loop() {
  oled.clearDisplay();
  oled.setFont( );
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  String ts = get_timestamp();
  oled.setCursor(0,0);
  oled.println(ts); 
  oled.display();
  delay(980);
  if ((millis()-tiempom)>=1000){ 
     myFile = SD.open("/esp32.txt", FILE_APPEND);
     myFile.println(ts);
     myFile.close();
     tiempom = millis();  
  }
  
    
 }
  






String get_timestamp(){
    DateTime  now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.day()   < 10)? "0" + String(now.day()  )   + " ": String(now.day())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}

